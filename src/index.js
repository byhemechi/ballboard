const express = require("express");
const app = express();

const scores = {};

app.get("/", (req, res) => {
    res.json(scores);
})

app.put("/:id", (req, res) => {
    try {
        const {id} = req.params;
        const {name, score} = req.headers;
        scores[id] = {name, score: Number(score)}
        res.json({name, score: Number(score)})
    } catch(err) {
        res.json(err)
    }
})

app.get("/identify", (req, res) => {
    res.json((Math.random().toString(36).replace(/0\./, "")))
})

app.listen(80, "0.0.0.0", (err) => console.log(80))
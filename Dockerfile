FROM node:lts
WORKDIR /usr/src/app
COPY src .
RUN yarn
EXPOSE 80
CMD ["yarn", "nodemon"]